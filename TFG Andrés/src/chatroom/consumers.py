import json
from django.contrib.auth import get_user_model
from channels.consumer import AsyncConsumer
from channels.db import database_sync_to_async
from .models import Room, ChatRoomMessage
from .views import RoomView

class ChatRoomConsumer(AsyncConsumer):
    async def websocket_connect(self, event):
        print("connected", event)
        
        room_name = self.scope['url_route']['kwargs']['room_name']
        room_private = self.scope['url_route']['kwargs']['room_private']
        user = self.scope['user']
        thread_obj = await self.get_thread(user, room_name, room_private)
        self.thread_obj = thread_obj
        chat_room = f"thread_{thread_obj.id}"
        self.chat_room = chat_room
        await self.channel_layer.group_add(
            chat_room,
            self.channel_name
        )

        await self.send({
            "type": "websocket.accept"
        })


    async def websocket_receive(self, event):
        # received {'type': 'websocket.receive', 'text': '{"message":"Hola"}'}
        print("received", event)
        front_text = event.get('text', None)
        if front_text is not None:
            loaded_dict_data = json.loads(front_text)
            msg = loaded_dict_data.get('message')
            user = self.scope['user']
            username = 'default'
            if user.is_authenticated:
                username = user.username
            myResponse = {
                'message': msg,
                'username': username
            }         
            await self.create_chat_message(user, msg)   
            # broadcast the message
            await self.channel_layer.group_send(
                self.chat_room,
                {
                    "type": "chat_message",
                    "text": json.dumps(myResponse)
                }
            )

    async def chat_message(self, event):
        # send the message
        await self.send({
            "type": "websocket.send",
            "text": event['text']
        })

    async def websocket_disconnect(self, event):
        print("disconnected", event)

    @database_sync_to_async
    def get_thread(self, user, room_name, room_private):
        return Room.objects.get_or_new(user, room_name, room_private)[0]

    @database_sync_to_async
    def create_chat_message(self, me, msg):
        thread_obj = self.thread_obj
        return ChatRoomMessage.objects.create(room=thread_obj,user=me,message=msg)
