from django import forms
from django.contrib.auth.models import User
from django.forms import ModelForm
from .models import Member

class ComposeForm(forms.Form):
    message = forms.CharField(
            widget=forms.TextInput(
                attrs={"class": "form-control"}
                )
            )