from django.apps import AppConfig


class ChatroomConfig(AppConfig):
    name = 'chatroom'

class RoomConfig(AppConfig):
    name = 'users'

    def ready(self):
        import users.signals
