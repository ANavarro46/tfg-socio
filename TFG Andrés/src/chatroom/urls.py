from django.urls import path, re_path
from .views import RoomView, InboxView, NotificationView, RoomDeleteView

app_name = 'chatroom'
urlpatterns = [
    path('', InboxView.as_view(), name='home'),
    path('notification/', NotificationView.as_view(), name='notification'),
    re_path(r"^(?P<room_name>[\w.@+-]+)/(?P<room_private>[\w.@+-]+)", RoomView.as_view(), name='room_url'),
    path('<int:pk>/delete/', RoomDeleteView.as_view(), name='room-delete'),
]