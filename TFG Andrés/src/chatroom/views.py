from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.http import Http404, HttpResponseForbidden
from django.shortcuts import render
from django.urls import reverse, resolve
from django.views.generic.edit import FormMixin
from django.contrib.auth.models import User
from django.views.generic import DetailView, ListView, CreateView, DeleteView

from .forms import ComposeForm
from .models import ChatRoomMessage, Room, Member
from django.contrib.auth import login
from django.http import HttpResponseRedirect
import requests
from django.shortcuts import render_to_response
from .models import Room

class RoomDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Room
    success_url = '/chatroom/'

    def test_func(self):
        room = self.get_object()
        if self.request.user == room.author:
            return True
        return False

class InboxView(LoginRequiredMixin, ListView):
    template_name = 'chatroom/inbox.html'
    rooms = Room.objects.get_all()
    def get_queryset(self):
        return {
            'rooms': Room.objects.get_all()
        }

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        data['rooms'] = Room.objects.get_all()
        return data

class NotificationView(LoginRequiredMixin, ListView):
    template_name = 'chatroom/notification.html'
    rooms = Room.objects.get_all()
    def get_queryset(self):
        return {
            'rooms': Room.objects.get_all()
        }

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        data['rooms'] = Room.objects.get_all()
        data['chats'] = ChatRoomMessage.objects.order_by('-timestamp')[:10]
        data['lastchats'] = []
        for room in Room.objects.get_all():
            chats = room.chatroommessage_set.order_by('-timestamp')[:1]
            data['lastchats'].append(chats)
        return data

class RoomView(LoginRequiredMixin, FormMixin, DetailView):
    template_name = 'chatroom/room.html'
    form_class = ComposeForm
    success_url = './'
    
    def get_queryset(self):
        return Room.objects.by_user(self.request.user)

    def get_object(self):
        room_name  = self.kwargs.get("room_name")
        room_private  = self.kwargs.get("room_private")
        obj, created    = Room.objects.get_or_new(self.request.user, room_name, room_private)        
        if obj == None:
            raise Http404
        return obj

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = self.get_form()
        return context

    def post(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return HttpResponseForbidden()
        self.object = self.get_object()
        form = self.get_form()
        if form.is_valid():
            return self.chat_form_valid(form)
        else:
            return self.form_invalid(form)

    def chat_form_valid(self, form):
        room = self.get_object()
        user = self.request.username
        message = form.cleaned_data.get("message")
        ChatRoomMessage.objects.create(user=user, room=room, message=message)
        return super().chat_form_valid(form)

def home(request):
    response = requests.get('http://freegeoip.net/json/')
    geodata = response.json()
    return render(request, 'core/home.html', {'ip': geodata['ip'],'country': geodata['country_name']})