from django.conf.urls import url
from channels.routing import ProtocolTypeRouter, URLRouter
from channels.auth import AuthMiddlewareStack
from channels.security.websocket import AllowedHostsOriginValidator, OriginValidator
from chat.consumers import ChatConsumer
from chatroom.consumers import ChatRoomConsumer

application = ProtocolTypeRouter({
    'websocket': AllowedHostsOriginValidator(
        AuthMiddlewareStack(
            URLRouter(
                [
                    url(r"^chat/(?P<username>[\w.@+-]+)", ChatConsumer, name='chat'),
                    url(r"^chatroom/(?P<room_name>[\w.@+-]+)/(?P<room_private>[\w.@+-]+)", ChatRoomConsumer, name='chatroom'),
                    url(r"^(?P<room_name>[\w.@+-]+)/(?P<room_private>[\w.@+-]+)/add_user", ChatRoomConsumer, name='chatroom_add'),
                ]
            )
        )
    )
})

