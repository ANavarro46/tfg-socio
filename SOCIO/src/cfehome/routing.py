from django.conf.urls import url
from channels.routing import ProtocolTypeRouter, URLRouter
from channels.auth import AuthMiddlewareStack
from channels.security.websocket import AllowedHostsOriginValidator, OriginValidator
from chat.consumers import ChatConsumer
from chatroom.consumers import ChatRoomConsumer

application = ProtocolTypeRouter({
    'websocket': AllowedHostsOriginValidator(
        AuthMiddlewareStack(
            URLRouter(
                [
                    url(r"^chat/(?P<username>[\w.@+-]+)", ChatConsumer, name='chat'),
                    url(r"^chatroom/(?P<room_name>[\w.@+-]+)/(?P<room_private>[\w.@+-]+)/(?P<room_channel>[\w.@+-]+)/(?P<room_author>[\w.@+-]+)/(?P<branch_name>[\w.@+-]+)", ChatRoomConsumer, name='chatroom'),
                    #url(r"^chatroom/(?P<branch_name>[\w.@+-]+)/(?P<room_name>[\w.@+-]+)/(?P<room_private>[\w.@+-]+)/(?P<room_channel>[\w.@+-]+)", ChatRoomConsumer, name='chatroom'),
                ]
            )
        )
    )
})

