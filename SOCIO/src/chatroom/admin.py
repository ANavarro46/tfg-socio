from django.contrib import admin


from .models import User, Room, ChatRoomMessage, Member

class ChatRoomMessage(admin.TabularInline):
    model = ChatRoomMessage

class RoomAdmin(admin.ModelAdmin):
    inlines = [ChatRoomMessage]
    users = [Member]
    class Meta:
        model = Room 

admin.site.register(Room, RoomAdmin)
