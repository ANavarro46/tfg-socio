# Generated by Django 2.1.7 on 2019-05-10 15:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('chatroom', '0006_auto_20190510_1736'),
    ]

    operations = [
        migrations.AlterField(
            model_name='room',
            name='private',
            field=models.CharField(choices=[('public', 'public'), ('private', 'private')], max_length=10),
        ),
    ]
