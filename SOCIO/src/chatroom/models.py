from django.db import models

from django.conf import settings
from django.db import models
from django.utils import timezone
from django.db.models import Q
from django.contrib.auth.models import User
from django.urls import reverse

class RoomManager(models.Manager):
    def by_user(self, room_name):
        qlookup = Q(name=room_name)
        qs = self.get_queryset().filter(qlookup)
        return qs

    def get_all(self):
        return self.all()

    def get_or_new(self, user, room_name, room_private, room_channel, branch_name, original_author): 
        qlookup = Q(name=room_name)
        qlookupbranch = Q(name=branch_name)
        if branch_name != 'room':
            qsbr = self.get_queryset().filter(qlookupbranch).distinct()
            if qsbr.count() == 1 and qsbr.first().private == 'private': 
                if qsbr.first().author == user or qsbr.first().member_set.filter(user=user):
                    return qsbr.first(), False
                return None, False
            elif qsbr.count() == 1 and qsbr.first().private == 'public':
                return qsbr.first(), False
            else:
                obj = Room(name=branch_name, author=user, private=room_private, channel=room_channel, branch=True, project=room_name, original_author=original_author)
                obj.save()
                all_users = User.objects.all()
                for usr in all_users:
                    obj.member_set.create(user=usr)
                obj.save()
                return obj, True
            return None, False
        else:
            qs = self.get_queryset().filter(qlookup).distinct()
            if qs.count() == 1 and qs.first().private == 'private': 
                if qs.first().author == user or qs.first().member_set.filter(user=user):
                    return qs.first(), False
                return None, False
            elif qs.count() == 1 and qs.first().private == 'public':
                return qs.first(), False
            else:
                # Se incluyen todos los usuarios, room publica
                obj = Room(name=room_name, author=user, private=room_private, channel=room_channel, original_author=original_author)
                obj.save()
                all_users = User.objects.all()
                for usr in all_users:
                    obj.member_set.create(user=usr)
                obj.save()
                return obj, True
            return None, False

class Room(models.Model):
    name = models.CharField(max_length=20)    
    author = models.ForeignKey(User, on_delete=models.CASCADE, null=True, related_name='chat_room_author')
    updated = models.DateTimeField(auto_now=True) 
    timestamp = models.DateTimeField(auto_now_add=True)
    private = models.CharField(max_length=10, null=False ,choices=( ("public", "public"),
                                                        ("private", "private")))
    channel = models.CharField(max_length=20, default="Socio-App")
    branch = models.BooleanField(default=False)
    project = models.CharField(max_length = 20, null=True)
    original_author = models.CharField(max_length = 20, null=True)

    objects = RoomManager()

    @property
    def room_group_name(self):
        return f'chat_{self.id}'

    def broadcast(self, msg=None):
        if msg is not None:
            broadcast_msg_to_chat(msg, group_name=self.room_group_name, user='admin')
            return True
        return False

    def get_absolute_url(self):
        return reverse('chatroom-home')

class Member(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)    
    room = models.ForeignKey(Room, on_delete=models.CASCADE)

class ChatRoomMessage(models.Model):
    #thread      = models.ForeignKey(Thread, null=True, blank=True, on_delete=models.SET_NULL)
    room        = models.ForeignKey(Room, null=True, blank=True, on_delete=models.CASCADE)
    user        = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name='sender', on_delete=models.CASCADE)
    message     = models.TextField()
    bot         = models.BooleanField(default=False)
    external    = models.BooleanField(default=False)
    timestamp   = models.DateTimeField(auto_now_add=True)

