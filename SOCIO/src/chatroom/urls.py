from django.urls import path, re_path
from .views import RoomView, InboxView, NotificationView, RoomDeleteView, OtherAppsView

app_name = 'chatroom'
urlpatterns = [
    path('', InboxView.as_view(), name='chatroom-home'),
    path('otherapps/', OtherAppsView.as_view(), name='otherapps-home'),
    path('notification/', NotificationView.as_view(), name='notification'),
    re_path(r"^(?P<room_name>[\w.@+-]+)/(?P<room_private>[\w.@+-]+)/(?P<room_channel>[\w.@+-]+)/(?P<room_author>[\w.@+-]+)/(?P<branch_name>[\w.@+-]+)", RoomView.as_view(), name='room_url'),
]