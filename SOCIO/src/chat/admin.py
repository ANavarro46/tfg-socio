from django.contrib import admin


from .models import Thread, ChatMessage, User

class ChatMessage(admin.TabularInline):
    model = ChatMessage

class ThreadAdmin(admin.ModelAdmin):
    inlines = [ChatMessage]
    users = [User]
    class Meta:
        model = Thread 


admin.site.register(Thread, ThreadAdmin)
