from django.urls import path, re_path
from .views import ThreadView, InboxView

app_name = 'chat'
urlpatterns = [
    re_path(r"^(?P<username>[\w.@+-]+)", ThreadView.as_view(), name='thread_url'),
    path('', InboxView.as_view(), name='home'),
]
